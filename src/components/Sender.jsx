import React from 'react';
import PropTypes from 'prop-types';




class Sender extends React.Component {


  //const Sender = props => {
  constructor(props) {
    super(props);
    this.state = { store: props.store}
   // props.store.subscribe(()=>{console.log(arguments)})
  }
  render() {
    return (<div ><button onClick={(evt) => {
      this.props.store.addMessage({text:'hello'});
      // apres l'ajout au store je veux que tous les omponent qui sappuie sur la valeur du store fassent leur render ou setState    
      console.log(this.props.store.message)
        
    }}>Add</button>
      {
        this.props.store.message.map((e,i)=><div>message {i} : {e.text}</div>)
        //JSON.stringify(this.props.store)
      }
    </div>)
  };
}
Sender.propTypes = {
  user: PropTypes.object.isRequired,
  store: PropTypes.object.isRequired,
};

export default Sender;
