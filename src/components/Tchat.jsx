import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Sender from './Sender.jsx';
import Viewer  from './Viewer';
// import Store from './store' 
import store from './store' 
 

 //const store=new Store();
class Tchat extends Component {
 
  constructor(props) {
    super(props);
    this.state = {};
  //  store.message.subscribe(this.setState);
  }
  render() {
    const { ME } = this.props;
 
    return (
      <div>
        <Sender ME={ME} store={store.message} />
        <Viewer ME={ME} store={store} />
      </div>
    );
  }
}

Tchat.propTypes = {
  user: PropTypes.string.isRequired,
};
 export default Tchat;