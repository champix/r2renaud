import React from 'react';
import PropTypes from 'prop-types';

const Viewer = props => <div className="tchat-viewer">viewer</div>;

Viewer.propTypes = {
  user: PropTypes.object,
  store: PropTypes.object.isRequired,
};

export default Viewer;
